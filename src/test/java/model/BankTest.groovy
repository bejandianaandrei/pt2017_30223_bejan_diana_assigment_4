package model

/**
 * Created by diana on 01.05.2017.
 */
class BankTest extends groovy.util.GroovyTestCase {
    Bank bank = new Bank();
    void testGetPersonHashMap() {
        boolean correct=false;
        bank.loadInfo();
        if (bank.getPersonHashMap().size()>0) correct=true;
        assert correct;
    }

    void testAddPerson() {
       int size= bank.getPersonHashMap().size();
        Person person=new Person("Diana", "Bejan","dianne", "dianne");
        bank.addPerson(person, 0.1,10);

        assert  size+1==bank.getPersonHashMap().size();
    }
    void testAddAssociateAcount() {
        Person person=new Person("Diana", "Bejan","dianne", "dianne");
        bank.addPerson(person, 0.1,10);
        int size= bank.viewInformation(person.getPersonID()).value.size();
        bank.addAssociateAccount(person.getPersonID(),new SpendingAccount(0));
        assert size+1==bank.viewInformation(person.getPersonID()).value.size();
    }
    void testRemoveAssociateAccount() {
        Person person=new Person("Diana", "Bejan","dianne", "dianne");
        bank.addPerson(person, 0.1,10);
        int size= bank.viewInformation(person.getPersonID()).value.size();

        bank.removeAssociateAccount(person.getPersonID(),bank.viewInformation(person.getPersonID()).value.get(0).getAccountID());
        assert size-1==bank.viewInformation(person.getPersonID()).value.size();
    }
    void testRemovePerson() {
        int size= bank.getPersonHashMap().size();
        Person person=new Person("Diana", "Bejan","dianne", "dianne");
        bank.addPerson(person, 0.1,10);
        assert bank.removePerson(person.getPersonID());
    }


}
