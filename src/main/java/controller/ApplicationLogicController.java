package controller;

import model.*;
import view.ViewController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by diana on 30.04.2017.
 */
public class ApplicationLogicController {
    private ViewController viewController;
    private Bank bank;

    public ApplicationLogicController() {
        viewController = new ViewController();
        bank = new Bank();
        bank.loadInfo();
        displayMainFrame();
        displayLoginPanel("");
    }

    private void displayMainFrame() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                viewController.showMainFrame();
            }
        });
    }

    public void displayLoginPanel(final String message) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                if (message.length() == 0) {
                    viewController.showLoginPanel();
                } else {
                    viewController.showLoginPanel(message);
                }
                addLoginPanelLoginButtonActionListener();
                addLoginPanelCloseButtonActionListener();
                addLoginPanelRegistrationButtonActionListener();
            }
        });
    }

    public void displayAdminPanel() {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {

                viewController.showAdminPanel(bank.getPersonHashMap());

                addAdminPanelDeleteUserButtonActionListener();
                addAdminPanelCloseButtonActionListener();
                addAdminPanelCancelButtonActionListener();
            }
        });
    }

    public void displayInformationPanel(final HashMap.Entry<Person, ArrayList<Account>> entry) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                viewController.showInformationPanel(entry);
                addInformationPanelAddSavingAccountButtonActionListener();
                addInformationPanelAddSpendingAccountButtonActionListener();
                addInformationPanelDeleteUserButtonActionListener();
                addInformationPanelGetInterestButtonActionListener();
                addInformationPanelWithdrawButtonActionListener();
                addInformationPanelDepositButtonActionListener();
                addInformationPanelBackButtonActionListener();
                addInformationPanelCloseButtonActionListener();
                addInformationPaneladdDeleteAccountButtonActionListener();
            }
        });
    }

    public void displayRegistrationPanel(final String message) {
        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                if (message.length() == 0) {
                    viewController.showRegistationPanel();
                    addRegistrationPanelConfirmButtonActionListener();
                    addRegistrationPanelExitButtonActionListener();
                    addRegistrationPanelCancelButtonActionListener();
                } else {
                    viewController.showRegistationPanel(message);
                }

            }
        });

    }

    public void addLoginPanelLoginButtonActionListener() {
        viewController.getLoginPanel().addLoginButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                String password = viewController.getLoginPanel().getPassword();
                String login = viewController.getLoginPanel().getLogin();
                if (password.equalsIgnoreCase("admin") & login.equalsIgnoreCase("admin")) {
                    displayAdminPanel();
                } else {
                    if (login.length() >= 6 & password.length() >= 6) {
                        for (HashMap.Entry<Person, ArrayList<Account>> entry : bank.getPersonHashMap().entrySet()) {
                            if (entry.getKey().getLogin().equalsIgnoreCase(login) &
                                    entry.getKey().getPassword().equalsIgnoreCase(password)) {
                                displayInformationPanel(entry);
                            }
                        }
                    } else {
                        displayLoginPanel("Something is wrong, try again");
                    }
                }
            }
        });

    }

    public void addLoginPanelCloseButtonActionListener() {
        viewController.getLoginPanel().addCloseButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                bank.saveInfo();
                viewController.closeMainFrame();
            }
        });

    }

    public void addLoginPanelRegistrationButtonActionListener() {
        viewController.getLoginPanel().addRegistrationButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                displayRegistrationPanel("");
            }
        });

    }

    public void addRegistrationPanelConfirmButtonActionListener() {
        viewController.getRegistrationPanel().addConfirmationButtonActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String firstname, lastname, login, password;
                int period;
                double interest;
                password = viewController.getRegistrationPanel().getPassword();
                login = viewController.getRegistrationPanel().getUsername();
                firstname = viewController.getRegistrationPanel().getFirstname();
                lastname = viewController.getRegistrationPanel().getLastname();
                period = viewController.getRegistrationPanel().getPeriod();
                interest = viewController.getRegistrationPanel().getInterest() / 100.0;
                if (login.length() >= 6 & password.length() >= 6 & lastname.length() > 2 & firstname.length() > 2 & period > 0 & interest > 0.0) {
                    Person person = new Person(firstname, lastname, login, password);
                    if (bank.addPerson(person, interest, period))
                        displayInformationPanel(bank.viewInformation(person.getPersonID()));
                    else displayRegistrationPanel("The username is taken. Try again");
                } else displayRegistrationPanel("Something is wrong. Try again");

            }


        });


    }

    public void addRegistrationPanelExitButtonActionListener() {
        viewController.getRegistrationPanel().addCloseButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                bank.saveInfo();
                viewController.closeMainFrame();
            }
        });

    }

    public void addRegistrationPanelCancelButtonActionListener() {
        viewController.getRegistrationPanel().addCancelButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                displayLoginPanel("");
            }
        });
    }

    public void addInformationPanelAddSavingAccountButtonActionListener() {
        viewController.getInformationPanel().addAddSavingAccountActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                double interest=viewController.getInformationPanel().getInterest()/100.0;
                int period=viewController.getInformationPanel().getPeriod();
                SavingAccount spendingAccount = new SavingAccount(interest, period, viewController.getInformationPanel().getUserID());
                bank.addAssociateAccount(viewController.getInformationPanel().getUserID(), spendingAccount);
                bank.saveInfo();
                displayInformationPanel(bank.viewInformation(viewController.getInformationPanel().getUserID()));
            }
        });

    }

    public void addInformationPanelAddSpendingAccountButtonActionListener() {
        viewController.getInformationPanel().addAddSpendingAccountActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                SpendingAccount spendingAccount = new SpendingAccount(viewController.getInformationPanel().getUserID());
                bank.addAssociateAccount(viewController.getInformationPanel().getUserID(), spendingAccount);
                bank.saveInfo();
                displayInformationPanel(bank.viewInformation(viewController.getInformationPanel().getUserID()));
            }
        });

    }

    public void addInformationPanelDeleteUserButtonActionListener() {
        viewController.getInformationPanel().addDeleteUserButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                bank.removePerson(viewController.getInformationPanel().getUserID());
                bank.saveInfo();
                displayLoginPanel("");
            }
        });

    }

    public void addInformationPanelGetInterestButtonActionListener() {
        viewController.getInformationPanel().addGetInterestButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                int userID = viewController.getInformationPanel().getUserID();
                int selectedAccount = viewController.getInformationPanel().getSelectedAccountID();
                if (selectedAccount != -1) {
                    for (HashMap.Entry<Person, ArrayList<Account>> entry : bank.getPersonHashMap().entrySet()) {

                        if (entry.getKey().getPersonID() == userID) {
                            for (Account account : entry.getValue())
                                if (account.getAccountID() == selectedAccount)
                                    account.addInterest();
                        }


                    }
                    bank.saveInfo();
                    displayInformationPanel(bank.viewInformation(viewController.getInformationPanel().getUserID()));
                } else JOptionPane.showMessageDialog(null, "I think you should select an account first");
            }
        });

    }

    public void addInformationPanelWithdrawButtonActionListener() {
        viewController.getInformationPanel().addWithdrawButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                int userID = viewController.getInformationPanel().getUserID();
                int selectedAccount = viewController.getInformationPanel().getSelectedAccountID();
                double sum = viewController.getInformationPanel().getSumField();
                if (selectedAccount != -1) {
                    for (HashMap.Entry<Person, ArrayList<Account>> entry : bank.getPersonHashMap().entrySet()) {

                        if (entry.getKey().getPersonID() == userID) {
                            for (Account account : entry.getValue())
                                if (account.getAccountID() == selectedAccount)
                                    account.withdraw(sum);
                        }


                    }
                    bank.saveInfo();
                    displayInformationPanel(bank.viewInformation(viewController.getInformationPanel().getUserID()));
                } else JOptionPane.showMessageDialog(null, "I think you should select an account first");
            }
        });

    }

    public void addInformationPanelDepositButtonActionListener() {
        viewController.getInformationPanel().addDepositButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                int userID = viewController.getInformationPanel().getUserID();
                int selectedAccount = viewController.getInformationPanel().getSelectedAccountID();
                double sum = viewController.getInformationPanel().getSumField();
                if (selectedAccount != -1) {
                    for (HashMap.Entry<Person, ArrayList<Account>> entry : bank.getPersonHashMap().entrySet()) {

                        if (entry.getKey().getPersonID() == userID) {
                            for (Account account : entry.getValue())
                                if (account.getAccountID() == selectedAccount)
                                    account.deposit(sum);
                        }


                    }
                    bank.saveInfo();
                    displayInformationPanel(bank.viewInformation(viewController.getInformationPanel().getUserID()));
                } else JOptionPane.showMessageDialog(null, "I think you should select an account first");
            }
        });


    }

    public void addInformationPaneladdDeleteAccountButtonActionListener() {
        viewController.getInformationPanel().addDeleteAccountButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                int userID = viewController.getInformationPanel().getUserID();
                int selectedAccount = viewController.getInformationPanel().getSelectedAccountID();
                if (selectedAccount != -1) {
                    for (HashMap.Entry<Person, ArrayList<Account>> entry : bank.getPersonHashMap().entrySet()) {

                        if (entry.getKey().getPersonID() == userID) {
                            for (Account account : entry.getValue())
                                if (account.getAccountID() == selectedAccount)
                                    bank.removeAssociateAccount(userID, selectedAccount);
                        }
                    }
                    bank.saveInfo();
                    displayInformationPanel(bank.viewInformation(userID));
                } else JOptionPane.showMessageDialog(null, "I think you should select an account first");
            }
        });

    }
    public void addAdminPanelDeleteUserButtonActionListener(){
        viewController.getAdminPanel().addDeleteUserButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                int userID =  viewController.getAdminPanel().getSelectedUserID();
                if (userID != -1) {
                    for (HashMap.Entry<Person, ArrayList<Account>> entry : bank.getPersonHashMap().entrySet()) {

                        if (entry.getKey().getPersonID() == userID) {
                                    bank.removePerson(userID);
                        }
                    }
                    bank.saveInfo();
                    displayAdminPanel();
                } else JOptionPane.showMessageDialog(null, "I think you should select an user first");
            }
        });
    }

    public void addInformationPanelBackButtonActionListener() {
        viewController.getInformationPanel().addBackButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                displayLoginPanel("");
            }
        });
    }

    public void addInformationPanelCloseButtonActionListener() {
        viewController.getInformationPanel().addCloseButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                bank.saveInfo();
                viewController.closeMainFrame();
            }
        });
    }

    public void addAdminPanelCloseButtonActionListener() {
        viewController.getAdminPanel().addCloseButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                bank.saveInfo();
                viewController.closeMainFrame();
            }
        });
    }

    public void addAdminPanelCancelButtonActionListener() {
        viewController.getAdminPanel().addCancelButtonActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                displayLoginPanel("");
            }
        });

    }
}
