package view;

import model.Account;
import model.Person;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by diana on 30.04.2017.
 */
public class ViewController {
    private JFrame mainFrame;
    private LoginPanel loginPanel;
    private InformationPanel informationPanel;
    private RegistrationPanel registrationPanel;
    private AdminPanel adminPanel;

    public ViewController() {
        mainFrame = new JFrame();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(500, 500);
        mainFrame.setResizable(false);
        mainFrame.setTitle("Banking Application Login");
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setLayout(new GridLayout(1, 1));
    }

    public void showMainFrame() {
        mainFrame.getContentPane().removeAll();
        mainFrame.repaint();
        mainFrame.setVisible(true);

    }

    public void closeMainFrame() {
        mainFrame.setVisible(false);
        mainFrame.dispose();
    }

    public void showLoginPanel() {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        mainFrame.setTitle("Banking Application Login");
        loginPanel = new LoginPanel();
        mainFrame.add(loginPanel);
        mainFrame.pack();
    }

    public void showLoginPanel(String message) {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        mainFrame.setTitle("Banking Application Login");
        loginPanel.setError(message);
        mainFrame.add(loginPanel);
        mainFrame.pack();
    }

    public void showRegistationPanel() {
        mainFrame.getContentPane().removeAll();
        mainFrame.revalidate();
        mainFrame.setTitle("Banking Application Registration");
        registrationPanel = new RegistrationPanel();
        mainFrame.add(registrationPanel);
        mainFrame.pack();
    }

    public void showRegistationPanel(String message) {
        mainFrame.getContentPane().removeAll();
        mainFrame.setTitle("Banking Application Registration");
        mainFrame.revalidate();
        registrationPanel.setErorLabel(message);
        mainFrame.add(registrationPanel);
        mainFrame.pack();
    }

    public void showInformationPanel(HashMap.Entry<Person, ArrayList<Account>> personEntry) {
        mainFrame.getContentPane().removeAll();
        mainFrame.setTitle("Banking Application My Information");
        mainFrame.revalidate();
        informationPanel = new InformationPanel(personEntry);
        mainFrame.add(informationPanel);
        mainFrame.pack();
    }

    public void showAdminPanel(HashMap<Person, ArrayList<Account>> hashMap) {
        mainFrame.getContentPane().removeAll();
        mainFrame.setTitle("Banking Application Admin");
        mainFrame.revalidate();
        adminPanel = new AdminPanel(hashMap);
        mainFrame.add(adminPanel);
        mainFrame.pack();
    }

    public JFrame getMainFrame() {
        return this.mainFrame;
    }

    public LoginPanel getLoginPanel() {
        return loginPanel;
    }

    public AdminPanel getAdminPanel() {
        return adminPanel;
    }

    public InformationPanel getInformationPanel() {
        return informationPanel;
    }

    public RegistrationPanel getRegistrationPanel() {
        return registrationPanel;
    }
}
