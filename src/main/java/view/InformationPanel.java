package view;

import model.Account;
import model.Person;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by diana on 30.04.2017.
 */
public class InformationPanel extends JPanel {
    private JButton backButton, closeButton, addSavingAccount, addSpendingAccount, deleteUserButton,
            getInterestButton, withdrawButton, depositButton, deleteAccount;
    private JTable userInfo, accountsTable;
    private JPanel accountsPanel, userInfoPanel, buttonsPanel;
    private JTextField sumField;
    private JLabel sumLabel;
    private JSlider   periodSlider, interestSlider;


    public InformationPanel(HashMap.Entry<Person, ArrayList<Account>> personEntry) {
        Font bd = new Font("Monotype Corsiva", Font.BOLD, 12);
        Font cg = new Font("Georgia", Font.ITALIC, 12);

        UIManager.put("Label.font", bd);
        UIManager.put("Button.font", cg);
        UIManager.put("TextField.font", cg);
        setPreferredSize(new Dimension(500, 500));
        setBackground(new Color(20, 0, 20));
        setLayout(new BorderLayout());
        setForeground(new Color(185, 180, 255));
        accountsPanel = new JPanel(new GridLayout(2, 1));
        accountsPanel.setForeground(new Color(185, 180, 255));
        userInfoPanel = new JPanel(new GridBagLayout());
        userInfoPanel.setForeground(new Color(185, 180, 255));
        sumLabel = new JLabel("Sum");
        sumLabel.setForeground(new Color(185, 180, 255));
        deleteUserButton = new JButton("Delete User");
        backButton = new JButton("Back");
        closeButton = new JButton("Close");
        addSavingAccount = new JButton("Add SavingAccount");
        addSpendingAccount = new JButton("Add SpendingAccount");
        deleteAccount = new JButton("Delete Account");
        getInterestButton = new JButton("Get Interest");
        withdrawButton = new JButton("WithDraw");
        depositButton = new JButton("Deposit");
        deleteUserButton.setBackground(new Color(185, 180, 255));
        backButton.setBackground(new Color(185, 180, 255));
        closeButton.setBackground(new Color(185, 180, 255));
        addSavingAccount.setBackground(new Color(185, 180, 255));
        addSpendingAccount.setBackground(new Color(185, 180, 255));
        deleteAccount.setBackground(new Color(185, 180, 255));
        getInterestButton.setBackground(new Color(185, 180, 255));
        withdrawButton.setBackground(new Color(185, 180, 255));
        depositButton.setBackground(new Color(185, 180, 255));
        backButton.setSize(new Dimension(80, 20));
        closeButton.setPreferredSize(new Dimension(80, 20));
        addSavingAccount.setPreferredSize(new Dimension(80, 20));
        addSpendingAccount.setPreferredSize(new Dimension(80, 20));
        deleteAccount.setPreferredSize(new Dimension(80, 20));
        deleteUserButton.setPreferredSize(new Dimension(80, 20));
        getInterestButton.setPreferredSize(new Dimension(80, 20));
        withdrawButton.setPreferredSize(new Dimension(80, 20));
        depositButton.setPreferredSize(new Dimension(80, 20));
        sumField = new JTextField();
        sumField.setPreferredSize(new Dimension(100, 25));
        periodSlider = new JSlider(JSlider.HORIZONTAL, 0, 300, 150);

        periodSlider.setMajorTickSpacing(50);
        interestSlider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        interestSlider.setMajorTickSpacing(10);
        interestSlider.setMinorTickSpacing(5);
        Object columnNames[] = new Object[personEntry.getKey().getClass().getDeclaredFields().length];
        Object rowData[][] = new Object[1][personEntry.getKey().getClass().getDeclaredFields().length];
        Field field[] = personEntry.getKey().getClass().getDeclaredFields();
        int i = 0;
        for (Field f : field) {
            f.setAccessible(true);
            columnNames[i] = f.getName();
            try {
                rowData[0][i] = f.get(personEntry.getKey());
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            i++;
        }
        userInfo = new JTable(rowData, columnNames);
        columnNames = new Object[personEntry.getValue().get(0).getClass().getDeclaredFields().length - 1];
        rowData = new Object[personEntry.getValue().size()][personEntry.getValue().get(0).getClass().getDeclaredFields().length - 1];
        field = personEntry.getValue().get(0).getClass().getDeclaredFields();
        i = 0;

        userInfoPanel.setLayout(new BorderLayout());
        userInfoPanel.setPreferredSize(new Dimension(450, userInfo.getRowHeight() * 2));

        userInfo.setPreferredSize(new Dimension(450, userInfo.getRowHeight() * 2));

        userInfoPanel.add(userInfo, BorderLayout.CENTER);
        userInfoPanel.add(userInfo.getTableHeader(), BorderLayout.NORTH);
        userInfoPanel.setBackground(new Color(20, 0, 20));
        int at = 0;
        field = personEntry.getValue().get(at).getClass().getDeclaredFields();
        for (Field f : field) {
            if (!f.getName().contains("$")) {
                f.setAccessible(true);
                columnNames[i] = f.getName();

                try {
                    for (int j = 0; j < personEntry.getValue().size(); j++)
                        if (personEntry.getValue().get(j).getClass().getName().length() == personEntry.getValue().get(0).getClass().getName().length()) {
                            rowData[j][i] = f.get(personEntry.getValue().get(j));

                        } else at = j;

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                i++;
            }
        }
        i = 0;

        field = personEntry.getValue().get(at).getClass().getDeclaredFields();

        for (Field f : field) {
            if (!f.getName().contains("$")) {
                f.setAccessible(true);
                columnNames[i] = f.getName();
                try {
                    for (int j = 0; j < personEntry.getValue().size(); j++)
                        if (personEntry.getValue().get(j).getClass().getName().length() == personEntry.getValue().get(at).getClass().getName().length()) {
                            rowData[j][i] = f.get(personEntry.getValue().get(j));
                        }

                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                i++;
            }
        }
        accountsTable = new JTable(rowData, columnNames);
        accountsTable.setPreferredSize(new Dimension(450, accountsTable.getRowHeight() * (personEntry.getValue().size() + 1)));

        accountsPanel.setPreferredSize(new Dimension(450, accountsTable.getRowHeight() * (personEntry.getValue().size() + 1)));
        accountsPanel.setBackground(new Color(20, 0, 20));
        accountsPanel.setLayout(new BorderLayout());
        accountsPanel.add(accountsTable, BorderLayout.CENTER);
        accountsPanel.add(accountsTable.getTableHeader(), BorderLayout.NORTH);
        add(userInfoPanel, BorderLayout.NORTH);

        add(accountsPanel, BorderLayout.CENTER);

        buttonsPanel = new JPanel(new GridLayout(6, 3));
        buttonsPanel.setForeground(new Color(185, 180, 255));
        interestSlider.setPreferredSize(new Dimension(180, 40));
        JLabel interestLabel = new JLabel("Interest");
        interestLabel.setForeground(new Color(185, 180, 255));
        interestSlider.setPaintTicks(true);
        interestSlider.setPaintLabels(true);
        buttonsPanel.add(interestSlider);
        buttonsPanel.add(interestLabel);
        buttonsPanel.add(new JLabel(" "));
        interestSlider.setPreferredSize(new Dimension(180, 40));
        JLabel periodLabel = new JLabel("Interest");
        periodLabel.setForeground(new Color(185, 180, 255));
        periodSlider.setPaintTicks(true);
        periodSlider.setPaintLabels(true);
        buttonsPanel.add(periodSlider);
        buttonsPanel.add(periodLabel);
        buttonsPanel.add(new JLabel(" "));
        buttonsPanel.add(sumLabel);
        buttonsPanel.add(sumField);
        buttonsPanel.add(new JLabel(" "));
        buttonsPanel.add(addSavingAccount);
        buttonsPanel.add(addSpendingAccount);
        buttonsPanel.add(deleteAccount);
        buttonsPanel.add(getInterestButton);
        buttonsPanel.add(withdrawButton);
        buttonsPanel.add(depositButton);
        buttonsPanel.add(backButton);
        buttonsPanel.add(closeButton);
        buttonsPanel.add(deleteUserButton);
        add(buttonsPanel, BorderLayout.SOUTH);

    }


    //    private JTable userInfo, accountsTable;
//    private JTextField sumField;
    public void addBackButtonActionListener(ActionListener actionListener) {
        backButton.addActionListener(actionListener);
    }

    public void addCloseButtonActionListener(ActionListener actionListener) {
        closeButton.addActionListener(actionListener);
    }

    public void addAddSavingAccountActionListener(ActionListener actionListener) {
        addSavingAccount.addActionListener(actionListener);
    }

    public void addAddSpendingAccountActionListener(ActionListener actionListener) {
        addSpendingAccount.addActionListener(actionListener);
    }

    public void addDeleteUserButtonActionListener(ActionListener actionListener) {
        deleteUserButton.addActionListener(actionListener);
    }

    public void addGetInterestButtonActionListener(ActionListener actionListener) {
        getInterestButton.addActionListener(actionListener);
    }

    public void addWithdrawButtonActionListener(ActionListener actionListener) {
        withdrawButton.addActionListener(actionListener);
    }

    public void addDepositButtonActionListener(ActionListener actionListener) {
        depositButton.addActionListener(actionListener);
    }

    public void addDeleteAccountButtonActionListener(ActionListener actionListener) {
        deleteAccount.addActionListener(actionListener);
    }

    public double getSumField() {
        if (sumField.getText().length() == 0)
            return 0.0;

        for (int i = 0; i < sumField.getText().length(); i++) {
            char c = sumField.getText().charAt(i);
            if (!Character.isDigit(c))
                return 0.0;
        }
        return Double.parseDouble(sumField.getText());
    }

    public int getUserID() {
        return Integer.parseInt(userInfo.getValueAt(0, 0).toString());
    }

    public int getSelectedAccountID() {
        if (accountsTable.getSelectedRow() != -1) {
            return Integer.parseInt(accountsTable.getValueAt(accountsTable.getSelectedRow(), 0).toString());
        }
        else return -1;
    }
    public int getPeriod() {
        return periodSlider.getValue();
    }

    public int getInterest() {
        return interestSlider.getValue();
    }

}
