package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by diana on 30.04.2017.
 */
public class RegistrationPanel extends JPanel {
    private JButton confirmpationButton, cancelButton, closeButton;
    private JTextField firstnameField, lastnameField, loginField;
    private JTextField passwordField;
    private JLabel firstnameLabel, lastnameLabel, loginLabel, passwordLabel, informationLabel, errorLabel;
    private GridBagConstraints gridBagConstraints;
    private JSlider interestSlider, periodSlider;

    public RegistrationPanel() {
        Font bd = new Font("Monotype Corsiva", Font.BOLD, 12);
        Font cg = new Font("Georgia", Font.ITALIC, 12);

        UIManager.put("Label.font", bd);
        UIManager.put("Button.font", cg);
        UIManager.put("TextField.font", cg);
        setPreferredSize(new Dimension(500, 500));
        setBackground(new Color(20, 0, 20));
        setLayout(new GridBagLayout());
        setForeground(new Color(185, 180, 255));

        confirmpationButton = new JButton("Confirm");
        confirmpationButton.setBackground(new Color(185, 180, 255));
        cancelButton = new JButton("Cancel");
        cancelButton.setBackground(new Color(185, 180, 255));
        closeButton = new JButton("Exit");
        closeButton.setBackground(new Color(185, 180, 255));
        firstnameField = new JTextField();
        firstnameField.setPreferredSize(new Dimension(200, 30));
        lastnameField = new JTextField();
        lastnameField.setPreferredSize(new Dimension(200, 30));
        loginField = new JTextField();
        loginField.setPreferredSize(new Dimension(200, 30));
        passwordField = new JPasswordField();
        passwordField.setPreferredSize(new Dimension(200, 30));
        errorLabel = new JLabel("                                                   ");
        errorLabel.setForeground(Color.RED);
        informationLabel = new JLabel("Information about your Saving account");
        informationLabel.setForeground(new Color(185, 180, 255));
        firstnameLabel = new JLabel("FirstName");
        firstnameLabel.setForeground(new Color(185, 180, 255));
        lastnameLabel = new JLabel("Lastname");
        lastnameLabel.setForeground(new Color(185, 180, 255));
        loginLabel = new JLabel("Username");
        loginLabel.setForeground(new Color(185, 180, 255));
        passwordLabel = new JLabel("Password");
        passwordLabel.setForeground(new Color(185, 180, 255));
        periodSlider = new JSlider(JSlider.HORIZONTAL, 0, 300, 150);

        periodSlider.setMajorTickSpacing(50);
        interestSlider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        interestSlider.setMajorTickSpacing(10);
        interestSlider.setMinorTickSpacing(5);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipady = 0;
        gridBagConstraints.ipadx = 0;
        gridBagConstraints.insets = new Insets(10, 10, 10, 10);
        loadComponents();

    }

    public void loadComponents() {
        add(firstnameLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        firstnameField.setPreferredSize(new Dimension(180, 30));
        add(firstnameField, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        add(lastnameLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        lastnameField.setPreferredSize(new Dimension(180, 30));
        add(lastnameField, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        add(loginLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        loginField.setPreferredSize(new Dimension(180, 30));
        add(loginField, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        passwordField.setPreferredSize(new Dimension(180, 30));
        add(passwordLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        add(passwordField, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        add(errorLabel, gridBagConstraints);
        gridBagConstraints.gridy++;
        add(informationLabel, gridBagConstraints);
        gridBagConstraints.gridy++;
        interestSlider.setPreferredSize(new Dimension(180, 40));
        JLabel interestLabel = new JLabel("Interest");
        interestLabel.setForeground(new Color(185, 180, 255));
        add(interestLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        interestSlider.setPaintTicks(true);
        interestSlider.setPaintLabels(true);
        add(interestSlider, gridBagConstraints);

        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        periodSlider.setPreferredSize(new Dimension(180, 40));
        JLabel periodLabel = new JLabel("Period");
        periodLabel.setForeground(new Color(185, 180, 255));
        ;
        add(periodLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        periodSlider.setPaintTicks(true);
        periodSlider.setPaintLabels(true);
        add(periodSlider, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        confirmpationButton.setPreferredSize(new Dimension(180, 30));
        add(confirmpationButton, gridBagConstraints);
        gridBagConstraints.gridx++;
        closeButton.setPreferredSize(new Dimension(180, 30));
        add(closeButton, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        JLabel problemLabel = new JLabel("Back?");
        problemLabel.setForeground(new Color(160, 130, 255));
        add(problemLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        cancelButton.setPreferredSize(new Dimension(180, 30));
        add(cancelButton, gridBagConstraints);

    }

    public void addCloseButtonActionListener(ActionListener actionListener) {
        closeButton.addActionListener(actionListener);
    }

    public void addCancelButtonActionListener(ActionListener actionListener) {
        cancelButton.addActionListener(actionListener);
    }

    public void addConfirmationButtonActionListener(ActionListener actionListener) {
        confirmpationButton.addActionListener(actionListener);
    }

    public String getFirstname() {
        return firstnameField.getText();
    }

    public String getLastname() {
        return lastnameField.getText();
    }

    public String getUsername() {
        return loginField.getText();
    }

    public String getPassword() {
        return passwordField.getText();
    }

    public int getPeriod() {
        return periodSlider.getValue();
    }

    public int getInterest() {
        return interestSlider.getValue();
    }

    public void setErorLabel(String message) {
        errorLabel.setText(message);
    }


}
