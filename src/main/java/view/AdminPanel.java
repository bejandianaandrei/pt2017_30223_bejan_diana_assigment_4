package view;

import model.Account;
import model.Person;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by diana on 01.05.2017.
 */
public class AdminPanel extends JPanel {
    private JTable usersTable;
    private JButton cancelButton, closeButton, deleteUserButton;

    public AdminPanel(HashMap<Person, ArrayList<Account>> personHashMap) {
        Font cg = new Font("Georgia", Font.ITALIC, 12);
        UIManager.put("Button.font", cg);
        setPreferredSize(new Dimension(500, 500));
        setBackground(new Color(20, 0, 20));
        setLayout(new BorderLayout());
        setForeground(new Color(185, 180, 255));

        closeButton = new JButton("Close");
        closeButton.setPreferredSize(new Dimension(160, 30));
        closeButton.setBackground(new Color(185, 180, 255));
        deleteUserButton = new JButton("DeleteUser");
        deleteUserButton.setPreferredSize(new Dimension(160, 30));
        deleteUserButton.setBackground(new Color(185, 180, 255));
        cancelButton = new JButton("Cancel");
        cancelButton.setBackground(new Color(185, 180, 255));
        cancelButton.setPreferredSize(new Dimension(160, 30));
        Object columnNames[] = new Object[5];
        ArrayList<Person> personArrayList = new ArrayList<Person>();
        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet())
            personArrayList.add(entry.getKey());
        if (personArrayList.size()!=0){
        Object rowData[][] = new Object[ personArrayList.size()][personArrayList.get(0).getClass().getDeclaredFields().length];
        Field field[] = personArrayList.get(0).getClass().getDeclaredFields();
        int i = 0;
        for (Field f : field) {
            int j = 0;
            for (Person person : personArrayList) {
                f.setAccessible(true);
                columnNames[i] = f.getName();
                try {
                    rowData[j][i] = f.get(person);
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                j++;
            }
            i++;
        }
        usersTable = new JTable(rowData, columnNames);
        add(usersTable.getTableHeader(), BorderLayout.NORTH);}
        else {
            usersTable = new JTable(5, 5);
            usersTable.setRowHeight(1);
        }
        add(usersTable, BorderLayout.CENTER);
        JPanel aux = new JPanel(new BorderLayout());
        aux.add(deleteUserButton, BorderLayout.WEST);
        aux.add(cancelButton, BorderLayout.CENTER);
        aux.add(closeButton, BorderLayout.EAST);
        add(aux, BorderLayout.SOUTH);

    }

    public void addCloseButtonActionListener(ActionListener actionListener) {
        closeButton.addActionListener(actionListener);
    }

    public void addCancelButtonActionListener(ActionListener actionListener) {
        cancelButton.addActionListener(actionListener);
    }
    public void addDeleteUserButtonActionListener(ActionListener actionListener) {
        deleteUserButton.addActionListener(actionListener);
    }
    public JTable getUsersTable(){
        return usersTable;
    }
    public int getSelectedUserID() {
        if (!(usersTable.getRowHeight()==1)&usersTable.getSelectedRow() != -1)

            return Integer.parseInt(usersTable.getValueAt(usersTable.getSelectedRow(), 0).toString());
         return -1;
    }

}
