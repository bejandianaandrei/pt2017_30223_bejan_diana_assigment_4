package view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Created by diana on 30.04.2017.
 */
public class LoginPanel extends JPanel {
    private JTextField loginField;
    private JTextField passwordField;
    private JLabel passwordLabel, loginLabel, erorLabel;
    private JButton registrationButton, closeButton, loginButton;
    private GridBagConstraints gridBagConstraints;

    public LoginPanel() {
        Font bd = new Font("Monotype Corsiva", Font.BOLD, 12);
        Font cg = new Font("Georgia", Font.ITALIC, 12);
        UIManager.put("Label.font", bd);
        UIManager.put("Button.font", cg);
        UIManager.put("TextField.font", cg);
        setPreferredSize(new Dimension(500, 500));
        setBackground(new Color(20, 0, 20));
        setLayout(new GridBagLayout());
        setForeground(new Color(185, 180, 255));

        loginField = new JTextField();
        passwordField = new JPasswordField();
        passwordLabel = new JLabel("Password");
        loginLabel = new JLabel("Username");
        passwordLabel.setForeground(new Color(185, 180, 255));

        loginLabel.setForeground(new Color(185, 180, 255));
        erorLabel = new JLabel("              ");
        erorLabel.setForeground(Color.RED);

        registrationButton = new JButton("Registration");
        registrationButton.setBackground(new Color(185, 180, 255));
        closeButton = new JButton("Close Application");
        closeButton.setBackground(new Color(185, 180, 255));
        loginButton = new JButton("LogIn");
        loginButton.setBackground(new Color(185, 180, 255));

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipady = 0;
        gridBagConstraints.ipadx = 0;
        gridBagConstraints.insets = new Insets(10, 10, 10, 10);
        loadComponents();
    }

    public void loadComponents() {
        add(loginLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        loginField.setPreferredSize(new Dimension(200, 30));
        passwordField.setPreferredSize(new Dimension(200, 30));
        add(loginField, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;

        add(passwordLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        add(passwordField, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        add(erorLabel, gridBagConstraints);
        gridBagConstraints.gridy++;
        add(loginButton, gridBagConstraints);
        gridBagConstraints.gridx++;
        add(closeButton, gridBagConstraints);
        gridBagConstraints.gridx--;
        gridBagConstraints.gridy++;
        JLabel problemLabel = new JLabel("Troubles?");
        problemLabel.setForeground(new Color(160, 130, 255));
        add(problemLabel, gridBagConstraints);
        gridBagConstraints.gridx++;
        add(registrationButton, gridBagConstraints);
    }

    public void addCloseButtonActionListener(ActionListener actionListener) {
        this.closeButton.addActionListener(actionListener);
    }

    public void addLoginButtonActionListener(ActionListener actionListener) {
        this.loginButton.addActionListener(actionListener);
    }

    public void addRegistrationButtonActionListener(ActionListener actionListener) {
        this.registrationButton.addActionListener(actionListener);
    }

    public void setError(String message) {
        erorLabel.setText(message);
    }

    public String getPassword() {

        return passwordField.getText();
    }

    public String getLogin() {
        return loginField.getText();
    }


}
