package model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by diana on 29.04.2017.
 */
public interface BankProc {
    HashMap<Person, ArrayList<Account>> getPersonHashMap();

    /**
     * Verify if user is well formed
     *
     * @param username
     * @return true [ if user have at least 2 accounts]
     */
    boolean isWellFormed(String username);

    /**
     * @param person
     * @param interst
     * @param period
     * @return true if successful, false otherwise
     * @pre person not null
     * @pre interest >=0
     * @pre period  >=0
     */
    boolean addPerson(Person person, double interst, int period);

    /**
     * @param personID
     * @return true if successful, false otherwise
     * @pre personID!=0
     */
    boolean removePerson(int personID);

    /**
     * @param personID
     * @param account
     * @return true if successful, false otherwise
     * @pre personID!=0
     * @pre Account!=null
     */
    boolean addAssociateAccount(int personID, Account account);

    /**
     * @param personID
     * @param accountID
     * @return true if successful, false otherwise
     * @pre personID!=0
     * @pre AccountID!=0
     */
    boolean removeAssociateAccount(int personID, int accountID);

    /**
     * @param personID
     * @return UserEntry
     * @pre personID!=0;
     */
    HashMap.Entry<Person, ArrayList<Account>> viewInformation(int personID);

    /**
     * @return true if successful, false otherwise
     * @pre personHashMap.size()!=0
     */
    boolean saveInfo();

    boolean loadInfo();

}
