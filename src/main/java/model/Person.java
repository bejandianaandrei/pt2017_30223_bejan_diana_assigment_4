package model;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by diana on 29.04.2017.
 */
public class Person implements Serializable, Observer {
    private int personID;
    private String firstname;
    private String lastname;
    private String login;
    private String password;

    public Person(String firstname, String lastname, String login, String password) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.login = login;
        this.password = password;
        this.personID = hashCode();
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getLogin() {
        return this.login;
    }

    public String getPassword() {
        return this.password;
    }

    public int getPersonID() {
        return this.personID;
    }

    public int hashCode() {
        int hash = 0;
        hash += firstname.hashCode();
        hash += lastname.hashCode();
        hash += login.hashCode();
        return hash;
    }

    public void update(Observable observable, Object o) {

        String message = (String) o;

        System.out.println("Notification       >     " + message);


    }


}
