package model;

import javax.swing.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by diana on 29.04.2017.
 */
public class Bank implements BankProc {

    private HashMap<Person, ArrayList<Account>> personHashMap;

    public Bank() {
        personHashMap = new HashMap<Person, ArrayList<Account>>();
    }

    public HashMap<Person, ArrayList<Account>> getPersonHashMap() {
        return this.personHashMap;
    }

    public boolean isWellFormed(String username) {
        boolean valid = true;
        if (personHashMap.isEmpty()) valid = false;

        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet()) {
            if (entry.getKey().getLogin().equalsIgnoreCase(username)) {
                if (entry.getValue().size() < 1) valid = false;
                break;
            }
        }
        return valid;
    }


    public boolean addPerson(Person person, double interest, int period) {
        ArrayList<Account> accounts;
        boolean added = true;
        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet()) {
            if (entry.getKey().getLogin().equalsIgnoreCase(person.getLogin())) {
                JOptionPane.showMessageDialog(null, "Person with this username  already exists.");
                added = false;
                return added;
            }
        }
        Account savingAccount = new SavingAccount(interest, period, person.getPersonID());
        savingAccount.addObserver(person);
        Account spendingAccount = new SpendingAccount(person.getPersonID());
        spendingAccount.addObserver(person);
        accounts = new ArrayList<Account>();
        accounts.add(savingAccount);
        accounts.add(spendingAccount);
        personHashMap.put(person, accounts);
        return added;
    }

    public boolean removePerson(int personID) {
        boolean removed = false;
        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet()) {
            if (entry.getKey().getPersonID() == personID) {
                personHashMap.remove(entry.getKey());
                removed = true;
                return removed;
            }
        }
        return removed;
    }

    public boolean addAssociateAccount(int personID, Account account) {
        boolean added = false;
        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet())
            if (entry.getKey().getPersonID() == personID) {
                for (Account account1 : entry.getValue())
                    if (account1.getAccountID() == account.getAccountID())
                        return false;
                account.addObserver(entry.getKey());
                entry.getValue().add(account);

                return true;
            }
        return added;
    }

    public boolean removeAssociateAccount(int personID, int accountID) {
        boolean removed = false;
        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet())
            if (entry.getKey().getPersonID() == personID) {
                for (Account account : entry.getValue())
                    if (account.getAccountID() == accountID) {
                        entry.getValue().remove(account);
                        removed = true;
                        return removed;
                    }
            }
        return removed;
    }

    public HashMap.Entry<Person, ArrayList<Account>> viewInformation(int personID) {
        for (HashMap.Entry<Person, ArrayList<Account>> entry : personHashMap.entrySet())
            if (entry.getKey().getPersonID() == personID)
                return entry;
        return null;
    }

    public boolean saveInfo() {

        boolean saved = false;
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("src/data.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this.personHashMap);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in data.ser\n");
            saved = true;
        } catch (IOException e) {

            e.printStackTrace();
        }
        return saved;
    }

    public boolean loadInfo() {
        boolean saved = false;
        try {
            FileInputStream fileIn = new FileInputStream("src/data.ser");
            File f = new File("src/data.ser");
            if (f.exists() && f.isFile() && f.length() != 0) {
                ObjectInputStream in = new ObjectInputStream(fileIn);
                this.personHashMap = (HashMap<Person, ArrayList<Account>>) in.readObject();
                for (HashMap.Entry<Person, ArrayList<Account>> entry : this.getPersonHashMap().entrySet()) {
                    for (int i = 0; i < entry.getValue().size(); i++) {
                        entry.getValue().get(i).addObserver(entry.getKey());
                    }
                }
                in.close();
                saved = true;
            }
            fileIn.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return saved;
    }
}
