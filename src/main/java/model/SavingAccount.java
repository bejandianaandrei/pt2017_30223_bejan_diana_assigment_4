package model;

import java.io.Serializable;

/**
 * Created by diana on 29.04.2017.
 */
public class SavingAccount extends Account implements Serializable {
    private int accountID;
    private final String accountType = "Saving";
    private int period;
    private double interest;
    private double balance;


    public SavingAccount(double interest, int period, int personID) {

        if (interest > 0.0 && interest <= 1.0)
            this.interest = interest;
        else this.interest = 0.1;
        if (period > 0 && period <= 365)
            this.period = period;
        else this.period = 90;
        setAccountID(accountType.hashCode() + personID + period + (int) Math.round(interest * 100));

    }

    @Override
    public boolean setAccountID(int accountID) {

        this.accountID = accountID;
        return true;
    }

    @Override
    public boolean setAmount(double amount) {
        assert amount > 0.0;
        assert this.balance == 0.0;
        if (balance < 1.0)
            balance = amount;
        super.setChanged();
        String message = "Set amount  " + amount + " in account " + accountType + " " + amount;
        notifyObservers(message);
        return true;
    }

    @Override
    public int getAccountID() {
        return accountID;
    }

    @Override
    public boolean deposit(double amount) {
        if (amount > 0.0 && balance < 1.0)
            balance = balance + amount;
        else {
            String message = "Your balance is "+balance+", you can deposit only if itis 0.0";
            this.setChanged();
            notifyObservers(message);
            return false;
        }
        String message = "Deposit  " + amount + ", account " + accountType + " " + amount;
        this.setChanged();
        notifyObservers(message);
        return true;
    }

    @Override
    public boolean withdraw(double amount) {
        assert balance > 0.0;
        assert Math.abs(amount - balance) < 1;
        if (balance == amount && amount > 0.0)
            balance -= amount;
        else  {
            String message = "Your balance is "+balance+" and your sum is"+amount+"they must be >0 and amount==balance";
            this.setChanged();
            notifyObservers(message);
            return false;
        }
        super.setChanged();
        String message = "Withdraw " + amount + " from account " + accountType + " " + amount;
        notifyObservers(message);
        return true;
    }

    @Override
    public double getBalance() {
        return balance;
    }

    @Override
    public int getPeriod() {
        return period;
    }

    @Override
    public double getInterest() {
        return interest;
    }

    @Override
    public String getAcountType() {
        return this.accountType;
    }

    public boolean addInterest() {
        balance += getBalance() * interest;
        super.setChanged();
        String message = "Add interest account " + accountType + " " + accountID;
        notifyObservers(message);
        return true;
    }


}
