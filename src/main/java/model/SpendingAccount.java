package model;

import java.io.Serializable;

/**
 * Created by diana on 29.04.2017.
 */
public class SpendingAccount extends Account implements Serializable {
    private int accountID;
    private final String accountType = "Spending";
    private int period = -1;
    private double interest = -1;
    private double balance;

    public SpendingAccount(int personID) {
        setAccountID(accountType.hashCode() + personID);
    }

    public int getAccountID() {
        return accountID;
    }

    public double getBalance() {
        return balance;
    }

    public String getAcountType() {
        return accountType;
    }

    public double getInterest() {
        return interest;
    }

    public int getPeriod() {
        return period;
    }

    public boolean setAccountID(int accountID) {
        this.accountID = accountID;
        return true;
    }

    public boolean setAmount(double amount) {
        assert amount > 0.0;
        balance = amount;
        return true;
    }

    public boolean deposit(double amount) {
        assert amount > 0.0;
        if (amount > 0.0)
            balance += amount;
        else  {
            String message = "Your amount is "+amount+", you can't deposit less than 0.0 ";
            this.setChanged();
            notifyObservers(message);
            return false;
        }
        super.setChanged();
        String message = "Deposit " + amount + ", account " + accountType + " " + amount;
        notifyObservers(message);
        return true;
    }

    public boolean withdraw(double amount) {
        assert amount <= balance;
        if (amount > 0.0 && amount <= balance)
            balance -= amount;
        else  {
            String message = "Your balance is "+balance+", you can't withdraw money if they don't exist ";
            this.setChanged();
            notifyObservers(message);
            return false;
        }
        super.setChanged();
        String message = "Withdraw " + amount + " from account " + accountType + " " + amount;
        notifyObservers(message);
        return false;
    }

    public boolean addInterest() {
        return false;
    }
}
