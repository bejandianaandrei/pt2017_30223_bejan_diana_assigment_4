package model;

import java.io.Serializable;
import java.util.Observable;

/**
 * Created by diana on 29.04.2017.
 */
public abstract class Account extends Observable implements Serializable {

    /**
     * Set the account's ID
     *
     * @param accountId [account's ID]
     */
    public abstract boolean setAccountID(int accountId);

    /**
     * Set the balance
     *
     * @param amount [account's ID]
     * @pre amount>0.0
     * @returntype boolean [if operation is successful]
     */
    public abstract boolean setAmount(double amount);

    /**
     * Get the account's ID
     *
     * @return accountID [the ID of the account]
     * @noparam
     */
    public abstract int getAccountID();

    /**
     * Add money to the account
     *
     * @param amount [the sum to deposit]
     * @return boolean [if operation is successful]
     */
    public abstract boolean deposit(double amount);

    /**
     * Withdraw money from the account
     *
     * @param amount [the sum to withdraw]
     * @return boolean [if operation is successful]
     * @pre amount<=balance
     */
    public abstract boolean withdraw(double amount);

    /**
     * Get the current balance
     *
     * @return double
     * @noparam
     */
    public abstract double getBalance();

    /**
     * Add money to the account
     *
     * @return String [Account type : Spending or Saving]
     * @noparam
     */
    public abstract String getAcountType();

    /**
     * Get the interest for Saving account, spending account return -1
     *
     * @return interest [The interest of SavingAccount]
     * @noparam
     */
    public abstract double getInterest();

    /**
     * Get the period for Saving account, spending account return -1
     *
     * @return period [The period of SavingAccount]
     * @noparam
     */
    public abstract int getPeriod();

    /**
     * Add interest if it is a Saving accont
     *
     * @return boolean [If operation is successful]
     * @noparam
     */
    public abstract boolean addInterest();

}
